import React, { Component } from 'react';
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import Box from "../../components/utility/box";
import PageHeader from '../../components/utility/pageHeader';
import { connect } from "react-redux";
import { notification } from "../../components";
import { createObject, adminLogger, getCurrentUser, getRoles, getAllObjects } from "../../helpers/parseHelper";
import { ActionBtn } from './style';
import {
    Input,
    Select,
    Form,
    TreeSelect,
    Tree,
} from 'antd';
import { DownOutlined } from '@ant-design/icons';
import config from "./config.js";


const className = config.className
const { Option } = Select;

class CreateComponent extends Component {
    formRef = React.createRef();
    countryNameRef = React.createRef();
    //valueRef = React.useRef();

    constructor(props) {
        super(props);
        this.state = {
            editorState: '',
            loading: false,
            iconLoading: false,
            adminLevel: 1,
            roles: [],
            country: [],
            area: [],
            areaData: [],
            areaName: '',
            parentInfo: [],
            types: [],
            typeData: [],
            parentAreaInfo: [],
            parentTypeInfo: [],
        };
    }

    getItems = (objectId, rawData) => {
        let temp = [];
        rawData.forEach(item => {
            if (objectId === item.parentID) {
                const tempObj = {
                    title: item.title,
                    value: item.value,
                    parentID: item.parentID,
                    objectId: item.objectId,
                }
                temp.push(tempObj)
            }
        })
        return temp
    };


    componentDidMount = async () => {
        const user = await getCurrentUser()
        const adminLevel = user.get('adminLevel')
        this.setState({ adminLevel })
        const roles = await getRoles()
        this.setState({ roles })
        const country = await getAllObjects('Country')
        this.setState({ country })
        const area = await getAllObjects('Area')
        this.setState({ areaData: area })
        const types = await getAllObjects('DataType')
        this.setState({ typeData: types, types: types })

        let temp = []
        //map data to TreeSelect Format
        let rawData = area.map(data => {
            return {
                name: data.name,
                title: data.name,
                value: data.objectId,
                parentID: data.parentID,
                objectId: data.objectId,
            }
        });

        let rootArea = rawData.filter(data => data.parentID === undefined);
        rawData.map(item => {
            const CurrentItem = rawData.filter(data => data.parentID === item.objectId)
            item.children = CurrentItem
            temp.push(item);
            //console.log('CurrentItem:', CurrentItem);
        })
        //console.log('New Data:', rootArea);
        this.setState({ area: rootArea });

        
         //map data  Types to TreeSelect Format
         let rawTypeData = types.map(data => {
            return {
                name: data.name,
                title: data.name,
                value: data.objectId,
                parentId: data.ParentId,
                objectId: data.objectId,
            }
        });
        let rootType = rawTypeData.filter(data => data.parentId === undefined);
        rawTypeData.map(item => {
            const CurrentItem = rawTypeData.filter(data => data.parentId === item.objectId)
            item.children = CurrentItem
            //console.log('CurrentItem:', CurrentItem);
        })
        this.setState({ types: rootType });
    }

    handleSubmit = async e => {
        const { country, areaData, typeData, parentAreaInfo, parentTypeInfo } = this.state
        const promise = this.formRef.current.validateFields()
        const values = await Promise.resolve(promise)
        //console.log('values',values)
        if (values) {
            // values.countryName = country.find(item => item.objectId === values.countryId).name || 'undefined'
            // values.areaName = areaData.find(item => item.objectId === values.areaId).name || 'undefined'
            // values.typeName = typeData.find(item => item.objectId === values.typeId).name || undefined
            values.countryName = country.filter(item => values.countryId.includes(item.objectId)).map(item => item.name)
            values.areaName = areaData.filter(item => values.areaId.includes(item.objectId)).map(item => item.name)
            values.typeName = typeData.find(item => item.objectId === values.typeId).name
            values.areaData = parentAreaInfo
            values.typeData = parentTypeInfo
            const res = await createObject(className, values)
            notification(res.type, res.msg)
            if (res.type == 'success') {
                await adminLogger(className, 'CREATE', res.object.toJSON())
                this.formRef.current.resetFields();
                this.props.onCreateSuccess();
            }
        }
    };

    handleCountryIdChange = (value) => {
        const { country, areaName } = this.state
        console.log('Selected:', value)
        country.map(item => {
            if (item.objectId === value) {
                console.log(item.name)
                this.setState({ areaName: item.name })
                const data = { countryName: item.name };
                this
                    .formRef
                    .current
                    .setFieldsValue(data)
            }
        }
        )
    };

     //Function Get All Parents Area
     getAllParents(parentID) {
        const { areaData } = this.state;
        let currentParentId = parentID //กำหนดคีย์ ParentId เริ่มต้น
        let parentData = [];
        while (currentParentId != undefined) {
            const parentInfo = areaData.find(item => item.objectId === currentParentId) || undefined
            if (parentInfo) {
                const Info = JSON.stringify(parentInfo);
                //console.log('ParentInfo:', parentInfo.name);
                currentParentId = parentInfo.parentID;
                parentData.push(parentInfo);
            } else {
                currentParentId = undefined;
                //console.log('ParentInfo:', currentParentId);
            }
        }

        let parentList = [];
        parentData.forEach(item => {
            parentList.push(item.name);
        });

        //console.log('ParentList:', parentList);

        return parentData;
    }

        //Function Get All Parents Type
        getAllParentsType(parentID) {
            const { typeData } = this.state;
            let currentParentId = parentID //กำหนดคีย์ ParentId เริ่มต้น
            let parentData = [];
            while (currentParentId != undefined) {
                const parentInfo = typeData.find(item => item.objectId === currentParentId) || undefined
                if (parentInfo) {
                    console.log('ParentInfo:', parentInfo.name);
                    currentParentId = parentInfo.ParentId;
                    parentData.push(parentInfo);
                } else {
                    currentParentId = undefined;
                    //console.log('ParentInfo:', currentParentId);
                }
            }
    
            let parentList = [];
            parentData.forEach(item => {
                parentList.push(item.name);
            });
    
            //console.log('ParentList:', parentList);
    
            return parentData;
        }

    onAreaSelect = (selectedKeys, info) => {
        //console.log('selectedKeys', info.objectId);
        const parentID = info.parentID;
        const parentAreaInfo = this.getAllParents(parentID);
        this.setState({ parentAreaInfo });
        const data = { areaName: info.name, areaId: info.objectId };
        this
            .formRef
            .current
            .setFieldsValue(data)
    };

    onTypeSelect = (selectedKeys, info) => {
        console.log('selectedKeys', info);
        const parentId = info.parentId;
        const parentTypeInfo = this.getAllParentsType(parentId);

        this.setState({ parentTypeInfo });
        const data = { typeName: info.name, typeId: info.objectId };
        this
            .formRef
            .current
            .setFieldsValue(data)
    };

    render() {
        const { country, area, areaName, roles, types } = this.state

        return (
            <Form
                {...formItemLayout}
                ref={this.formRef}
            >
                <Form.Item
                    name="name"
                    label="Name"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item
                    name="countryId"
                    label="Country"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Select mode="multiple" placeholder="Please select">
                        {country.map(item => (
                            <Option value={item.objectId} key={item.objectId}>{item.name}</Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item
                    name="areaId"
                    label="Area"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <TreeSelect
                        style={{
                            width: '100%',
                        }}
                        multiple={true}
                        dropdownStyle={{
                            maxHeight: 400,
                            overflow: 'auto',
                        }}
                        switcherIcon={<DownOutlined />}
                        treeData={area}
                        showSearch={false}
                        showLine
                        placeholder="Please select"
                        treeDefaultExpandAll
                        //onSelect={this.onAreaSelect}
                    />
                </Form.Item>
                <Form.Item
                    name="typeId"
                    label="Type"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <TreeSelect
                        style={{
                            width: '100%',
                        }}
                        dropdownStyle={{
                            maxHeight: 400,
                            overflow: 'auto',
                        }}
                        switcherIcon={<DownOutlined />}
                        treeData={types}
                        showSearch={false}
                        showLine
                        placeholder="Please select"
                        treeDefaultExpandAll
                        onSelect={this.onTypeSelect}
                    />
                </Form.Item>
                <Form.Item
                    name="description"
                    label="Description"
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <ActionBtn
                        type="primary"
                        htmlType="submit"
                        onClick={this.handleSubmit}
                        style={{
                            width: 120
                        }}>
                        Save
                    </ActionBtn>
                </Form.Item>
            </Form>
        )
    }
}

const mapStateToProps = state => ({ Auth: state.Auth });

export default connect(mapStateToProps)(CreateComponent);

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 7
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
};