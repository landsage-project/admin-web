const options = [
  {
    key: '',
    label: 'sidebar.dashboard',
    leftIcon: 'ion-arrow-graph-up-right',
  }, {
    key: 'station',
    label: 'Station',
    leftIcon: 'ion-radio-waves',
    children: [
      {
        key: 'station',
        label: 'Manage Station',
        leftIcon: 'ion-android-document',
      },
    ]
  }, {
    key: 'layers',
    label: 'Layers',
    leftIcon: 'ion-images',
    children: [{
      key: 'layers',
      label: 'Manage Layers'
    }]
  }, {
    key: 'admin',
    label: 'Options',
    leftIcon: 'ion-gear-a',
    children: [
      {
        key: 'admin',
        label: 'Admin User',
        leftIcon: 'ion-person-stalker',
      }, {
        key: 'role',
        label: 'Role Permission',
        leftIcon: 'ion-android-hand',
      }, {
        key: 'country',
        label: 'Country',
        leftIcon: 'ion-android-globe',
      }, {
        key: 'area', //Lower letter only 
        label: 'Area',
        leftIcon: 'ion-android-locate',
      }, {
        key: 'type', //Lower letter only 
        label: 'Data Type',
        leftIcon: 'ion-cog-outline',
      }, {
        key: 'layerIndex',
        label: 'Layer Index',
        leftIcon: 'ion-android-globe',
      }, {
        key: 'siteChannel',
        label: 'Site Channel',
        leftIcon: 'ion-android-globe',
      },
    ]
  }
];
export default options;
