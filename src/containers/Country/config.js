import moment from 'moment';
import React from 'react';
import {Tag} from 'antd'
const config = {
    className: "Country",
    defaultQueryKey: "name",
    listTitle: "Country List",
    filterOptions: [
        {
            value: "name",
            label: "Country Name"
        },{
            value: "countryCode",
            label: "Country Code"
        },
    ],
    columns: [
        {
            title: 'Country Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => sorter(a, b, 'name'),
        }, {
            title: 'Country Code',
            dataIndex: 'countryCode',
            key: 'countryCode',
            sorter: (a, b) => sorter(a, b, 'countryCode'),
        }, {
            title: 'Site Count',
            dataIndex: 'site',
            key: 'site',
            render: (data) => <Tag>{data}</Tag>
        }, {
            title: 'Station Count',
            dataIndex: 'station',
            key: 'station',
            render: (data) => <Tag>{data}</Tag>
        }, 
    ]
}

const sorter = (a, b, key) => {
    if (a[key] < b[key])
        return -1;
    if (a[key] > b[key])
        return 1;
    return 0;
}


export default config