import React, { Component } from 'react';
import LayoutWrapper from "../../components/utility/layoutWrapper.js";
import Box from "../../components/utility/box";
import PageHeader from '../../components/utility/pageHeader';
import { connect } from "react-redux";
import { notification } from "../../components";
import { getObjectWithId, uploadParseFile, createObject, adminLogger, getCurrentUser, getRoles, getAllObjects } from "../../helpers/parseHelper";
import { ActionBtn } from './style';
import {
    Input,
    InputNumber,
    Select,
    Form,
    Button,
    Space,
    TreeSelect,
    DatePicker,
} from 'antd';
import { MinusCircleOutlined, PlusOutlined, DownOutlined } from '@ant-design/icons';
import config from "./config.js";
import Upload, { getUploadFileValue } from '../../components/uploadFile'
import DropdownDate from '../../components/dropdownDate'
import qs from 'query-string'

const className = config.className
const { Option } = Select;
const { TreeNode } = TreeSelect;

class CreateComponent extends Component {
    formRef = React.createRef();
    countryNameRef = React.createRef();
    //valueRef = React.useRef();

    constructor(props) {
        super(props);
        this.state = {
            editorState: '',
            loading: false,
            iconLoading: false,
            adminLevel: 1,
            roles: [],
            country: [],
            area: [],
            areaData: [],
            areaName: '',
            parentInfo: [],
            types: [],
            typeData: [],
            parentAreaInfo: [],
            parentTypeInfo: [],
            layerIndex: [],
            layerData: null,
        };
    }
    componentDidMount = async () => {
        this.setState({ loading: true });
        const parse = await qs.parse(window.location.search)
        const { objectId } = parse
        const layerData = await getObjectWithId('LayerList', objectId);
        const json = layerData.toJSON();
        //console.log('station Data:', json);
        this.setState({ layerData: json });

        const user = await getCurrentUser()

        const roles = await getRoles()
        this.setState({ roles, user })

        this.setState({ loading: false });

    }

    handleSubmit = async e => {
        this.setState({ loading: true });
        const promise = this.formRef.current.validateFields();
        const { layerData, user } = this.state
        const values = await Promise.resolve(promise)
        console.log('Form Values', values)
        //console.log('areaData', areaData);
        //getCurrentUser and set current user value to formRef

        if (values) {
            const { countryId, countryName, areaId, areaName, typeId, typeName, layerIndex, objectId } = layerData

            values.layerId = objectId //Id of LayerList object
            values.admin = user
            values.adminId = user.id
            if (values.fileDate) {
                values.fileDate = values.fileDate.toDate();
            }

            if (values.fileData) {
                const file = await uploadParseFile(values.fileData[0]);
                values.fileData[0].url = file.url;
                values.fileName = values.fileData[0]['name'];
                values.fileUrl = file.url;
            }

            if (values.fileLegend) {
                const file = await uploadParseFile(values.fileLegend[0]);
                values.fileLegend[0].url = file.url;
                values.legendFileName = values.fileLegend[0]['name'];
                values.legendUrl = file.url;
            }

            if (values.fileClip) {
                const file = await uploadParseFile(values.fileClip[0]);
                values.fileClip[0].url = file.url;
                values.clipFileName = values.fileClip[0]['name'];
                values.clipUrl = file.url;
            }

            const res = await createObject(className, { ...values, countryId, countryName, areaId, areaName, typeId, typeName, layerIndex })
            notification(res.type, res.msg)
            if (res.type == 'success') {
                await adminLogger(className, 'CREATE', res.object.toJSON())
                this.formRef.current.resetFields();
                this.props.onCreateSuccess();
                this.setState({ loading: false });
            } else {
                this.setState({ loading: false });
            }
        }
    };

    //Function Get All Parents Area
    getAllParents(parentID) {
        const { areaData } = this.state;
        let currentParentId = parentID //กำหนดคีย์ ParentId เริ่มต้น
        let parentData = [];
        while (currentParentId != undefined) {
            const parentInfo = areaData.find(item => item.objectId === currentParentId) || undefined
            if (parentInfo) {
                const Info = JSON.stringify(parentInfo);
                //console.log('ParentInfo:', parentInfo.name);
                currentParentId = parentInfo.parentID;
                parentData.push(parentInfo);
            } else {
                currentParentId = undefined;
                //console.log('ParentInfo:', currentParentId);
            }
        }

        let parentList = [];
        parentData.forEach(item => {
            parentList.push(item.name);
        });

        //console.log('ParentList:', parentList);

        return parentData;
    }

    //Function Get All Parents Type
    getAllParentsType(parentID) {
        const { typeData } = this.state;
        let currentParentId = parentID //กำหนดคีย์ ParentId เริ่มต้น
        let parentData = [];
        while (currentParentId != undefined) {
            const parentInfo = typeData.find(item => item.objectId === currentParentId) || undefined
            if (parentInfo) {
                console.log('ParentInfo:', parentInfo.name);
                currentParentId = parentInfo.ParentId;
                parentData.push(parentInfo);
            } else {
                currentParentId = undefined;
                //console.log('ParentInfo:', currentParentId);
            }
        }

        let parentList = [];
        parentData.forEach(item => {
            parentList.push(item.name);
        });

        //console.log('ParentList:', parentList);

        return parentData;
    }

    onAreaSelect = (selectedKeys, info) => {
        console.log('selectedKeys', selectedKeys);
        const parentID = info.parentID;
        const parentAreaInfo = this.getAllParents(parentID);
        this.setState({ parentAreaInfo });
    };

    onTypeSelect = (selectedKeys, info) => {
        console.log('selectedKeys', info);
        const parentId = info.parentId;
        const parentTypeInfo = this.getAllParentsType(parentId);

        this.setState({ parentTypeInfo });
        const data = { typeName: info.name, typeId: info.objectId };
        this
            .formRef
            .current
            .setFieldsValue(data)
    };

    render() {

        return (
            <Form
                {...formItemLayout}
                ref={this.formRef}
                onFinishFailed={(errorInfo) => {
                    //console.log('Failed:', errorInfo);
                    this.setState({ loading: false });
                }}
            >
                <Form.Item
                    name="name"
                    label="Name"
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>

                <Form.Item
                    name="north"
                    label="North"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <InputNumber
                        style={{ width: 200 }}
                    />
                </Form.Item>
                <Form.Item
                    name="south"
                    label="South"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <InputNumber
                        style={{ width: 200 }}
                    />
                </Form.Item>
                <Form.Item
                    name="east"
                    label="East"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <InputNumber
                        style={{ width: 200 }}
                    />
                </Form.Item>
                <Form.Item
                    name="west"
                    label="West"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <InputNumber
                        style={{ width: 200 }}
                    />
                </Form.Item>
                <Form.Item
                    name="fileDate"
                    label="Date"
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <DatePicker />
                </Form.Item>
                <Form.Item name="fileData" label="Layer File" valuePropName="fileList" getValueFromEvent={getUploadFileValue}
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}
                >
                    <Upload />
                </Form.Item>
                <Form.Item name="fileLegend" label="Legend File" valuePropName="fileList" getValueFromEvent={getUploadFileValue}
                    rules={[{
                        required: true,
                        message: 'Please fill data!'
                    }
                    ]}
                >
                    <Upload />
                </Form.Item>
                <Form.Item name="fileClip" label="Clip File" valuePropName="fileList" getValueFromEvent={getUploadFileValue}>
                    <Upload />
                </Form.Item>
                <Form.Item
                    name="adminID"
                    label="Admin ID"
                    style={{ display: 'none' }}
                    rules={[{
                        required: false,
                        message: 'Please fill data!'
                    }
                    ]}>
                    <Input />
                </Form.Item>

                <Form.Item {...tailFormItemLayout}>
                    <ActionBtn
                        type="primary"
                        htmlType="submit"
                        loading={this.state.loading}
                        onClick={this.handleSubmit}
                        style={{
                            width: 120
                        }}>
                        Save
                    </ActionBtn>
                </Form.Item>
            </Form>
        )
    }
}

const mapStateToProps = state => ({ Auth: state.Auth });

export default connect(mapStateToProps)(CreateComponent);

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 7
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 14
        }
    }
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        },
        sm: {
            span: 16,
            offset: 8
        }
    }
};